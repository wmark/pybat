CXXFLAGS=-Os
PYVER=$(shell python --version 2>&1 | cut -f 2 -d " " | cut -b -3)

clean:
	rm -r build *.so *.o *.pyc *.pyd *.pyo

py:
	-mkdir -p build/python
	g++ $(CXXFLAGS) -c -fPIC mmarray_py.cpp -o build/mmarray_py.o -I/usr/include/python$(PYVER)
	g++ $(CXXFLAGS) -shared -Wl,-soname,mmarray.so -o build/python/MappedStructures.so build/mmarray_py.o -lpython$(PYVER) -lboost_python

test:
	-mkdir -p build/test
	g++ $(CXXFLAGS) MMArrayTest.cpp -obuild/test/MMArray -lboost_unit_test_framework
	build/test/MMArray -p -l message

strip:
	find build/ -executable -type f -exec strip '{}' \;
