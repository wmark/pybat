"""
PyBAT - naive BAT implementation in Python

    by W-Mark Kubacki; wmark@hurrikane.de
    licensed under the terms of the RPL for non-commercial usage
"""

class intBAT(list):
    """Think of this BAT as a static list or expandable array which does
    not actually delete its contents but collects such deletes in another
    list or BAT."""

    """Collects which entries have been deleted and shall be skipped."""
    deletes = []

    def idx(self, i):
        if len(self.deletes) == 0:
            return i
        elif i < self.deletes[0]:
            return i
        elif i > self.deletes[-1]:
            return i + len(self.deletes)
        else:
            offset = 0
            for e in self.deletes:
                if e <= i:
                    offset += 1
                else:
                    break
            return i + offset

    def __delitem__(self, i):
        self.deletes.append(self.idx(i))
        self.deletes.sort()

    def __setitem__(self, i, x):
        j = self.idx(i)
        return list.__setitem__(self, j, x)

    def __getitem__(self, i):
        j = self.idx(i)
        return list.__getitem__(self, j)

    def __getslice__(self, i, j):
        slice = list.__getslice__(self, self.idx(i), self.idx(j))
        if len(slice) != abs(j-i):
            todel = [x-i for x in self.deletes if x > i or x < j]
            for x in todel:
                del slice[x]
        return slice

    def __len__(self):
        return list.__len__(self) - len(self.deletes)

    def __iter__(self):
        i = 0
        for e in list.__iter__(self):
            if not i in self.deletes:
                yield e
            i += 1

    def __contains__(self, x):
        for e in self:
            if x == e:
                return True
        return False

if __name__ == "__main__":
    bat = intBAT()

    for i in xrange(16):
        bat.append(i * 10)
    assert len(bat) == 16, "BAT does not return correct length"

    x = bat[3]
    y = bat[10]
    assert x != y, "values are equal, but shouldn't"
    assert x == bat[3], "reading twice doesn't result in same value"

    y = bat[4]
    del bat[3] # the value we stored in x
    assert y == bat[3], "deleting a value doesn't result in index re-numbering"
    assert len(bat) == 15, "BAT does not return correct length after deletion of an item"

    x = bat[3]
    bat[3] += 1
    assert x + 1 == bat[3], "BAT doesn't handle arithmetic operations correctly"
    bat[3] -= 1

    assert [0, 10, 20, 40, 50] == bat[0:5], "returned slice is wrong"

    for x in bat:
        assert x != 30, "a deleted item is still present in iteration"

    assert not 30 in bat, "'in' predicate still says the deleted item is there"
    bat.append(30)
    assert 30 in bat, "'in' fails on duplicate entries"

    print "END"
