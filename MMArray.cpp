#ifndef MAPPED_STRUCTURES_HPP
#define MAPPED_STRUCTURES_HPP

#include<cstdlib>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include<iostream>
#include<cstring>
#include<exception>

const unsigned int PAGESIZE = sysconf(_SC_PAGESIZE);

namespace MappedStructures {

struct ExplainedException : std::exception
{
	std::string explanation;
	ExplainedException(std::string why) throw() {
		explanation = why;
	}
	~ExplainedException() throw() {
		;
	}
};

struct FatalError : ExplainedException
{
	FatalError(std::string why) throw() : ExplainedException(why) {};
};
struct IndexError : ExplainedException
{
	IndexError(std::string why) throw() : ExplainedException(why) {};
};

typedef struct {
	unsigned int		data_at;		// = sizeof(MMArrayPrefix)
	unsigned short		file_version;
	size_t			element_size;
	unsigned long		length;
} MMArrayPrefix;

/**
 * Expandable array in memmory mapped file.
 *
 */
template<class T> class MMArray
{
private:
	/** The file, mapped to memory. */
	char *map;

	/** Pointer to the file prefix. */
	MMArrayPrefix *m_prefix;

	/** The actual data, part of 'map' */
	T *m_data;

	/** File descriptor */
	int fd;

	/** Pointer to the filename. */
	const char *m_fname;

	/**
	 * Real size of the underlying file and thus memory size.
	 *
	 * Don't mix that with capacity, which is realsize/m_element_size.
	 */
	unsigned long realsize;

	void determine_current_filesize() {
		struct stat buf;
		fstat(fd, &buf);
		realsize = buf.st_size;
	}

	void resize_file_to(unsigned long newsize) {
		int result = lseek(fd, newsize - 1, SEEK_SET);
		if (result == -1) {
			close(fd);
			throw FatalError("Error calling lseek() to 'stretch' the file");
		}
		result = write(fd, "", 1);
		if (result != 1) {
			close(fd);
			throw FatalError("Error writing last byte of the file");
		}
		determine_current_filesize();
	}

	static
	bool is_magnitude_of_pagesize(unsigned long size) {
		return (size % PAGESIZE) == 0;
	}

	void adjust_map_to_new_size(size_t old_size) {
		map = static_cast<char*>(mremap(map, old_size, realsize, MREMAP_MAYMOVE));
		if (map == MAP_FAILED) {
			close(fd);
			throw FatalError("Error on mremap");
		}
		m_prefix = reinterpret_cast<MMArrayPrefix *>(map);
		m_data = reinterpret_cast<T *>(map + m_prefix->data_at);
	}

	void align_file_to_pagesize() {
		unsigned int new_size = (realsize / PAGESIZE + 1) * PAGESIZE;
		resize_file_to(new_size);
	}

protected:

	void initialize_new_file() {
		m_data = reinterpret_cast<T *>(map + sizeof(MMArrayPrefix));
		m_prefix->data_at = sizeof(MMArrayPrefix);
		m_prefix->file_version = 0;
		m_prefix->element_size = sizeof(T);
		m_prefix->length = 0;
	}

	void initialize_from_old_file() {
		m_data = reinterpret_cast<T *>(map + m_prefix->data_at);
	}

public:

	MMArray(const char *fname) {
		bool is_new_file = false;
		fd = open(fname, O_RDWR);
		if (fd == -1) {
			// create new file, open it again
			fd = open(fname, O_RDWR | O_CREAT, (mode_t)0600);
			is_new_file = true;
		}
		m_fname = fname;
		if (fd == -1) {
			throw FatalError("Error opening file for writing");
		}
		determine_current_filesize();
		if (is_new_file || realsize < PAGESIZE) {
			align_file_to_pagesize();
		}
		// do the actual memory mapping
		map = static_cast<char*>(mmap(0, realsize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
		if (map == MAP_FAILED) {
			close(fd);
			throw FatalError("Error mmapping the file");
		}
		m_prefix = reinterpret_cast<MMArrayPrefix *>(map);
		if (is_new_file) {
			initialize_new_file();
		} else {
			initialize_from_old_file();
		}
	}

	const char* get_filename() {
		return m_fname;
	}

	void set(unsigned long n, const T& what) {
		if (n > capacity() - 1) {
			throw IndexError("out of bounds - try expand() first or call append()");
		}
		if (m_prefix->length < n + 1) {
			m_prefix->length = n + 1;
		}
		m_data[n] = what;
	}

	T get(unsigned long n) {
		return at(n);
	}

	T& at(unsigned long n) {
		if (length() == 0 || n > length() - 1) {
			throw IndexError("out of bounds - try expand(), append() or write to the field first");
		}
		return m_data[n];
	}

	T& operator[] (unsigned long n) {
		return m_data[n];
	}

	/**
	 * Appends the given element.
	 *
	 * Expands available space if necessary.
	 */
	void append(const T& what) {
		if (length() == capacity()) {
			expand(capacity() + 1);
		}
		set(length(), what);
	}

	void finish() {
		if (munmap(map, realsize) == -1) {
			close(fd);
			throw FatalError("Error un-mmapping the file");
		}
		close(fd);
	}

	/**
	 * Expands available space so it can fit the by 'new_capacity' given amount of elements.
	 *
	 * Resulting capacity can be larger than the requested one, because the new size will be
	 * made a magnitude of system's page size.
	 */
	unsigned long expand(unsigned long new_capacity) {
		if (new_capacity * m_prefix->element_size > capacity()) {
			unsigned long 	old_size = realsize,
					new_realsize = new_capacity * sizeof(T) + sizeof(MMArrayPrefix);
			if (!is_magnitude_of_pagesize(new_realsize)) {
				new_realsize = (new_realsize / PAGESIZE + 1) * PAGESIZE;
			}
			resize_file_to(new_realsize);
			adjust_map_to_new_size(old_size);
		}
		return capacity();
	}

	/**
	 * Tells how many elements can currently be stored without expanding the space.
	 *
	 * If the allocated memory allows storing X elements, then this function returns X -
	 * even if only less than X elements are actually being stored.
	 *
	 * @return number of elements which at most fit in the allocated space
	 */
	unsigned long capacity() {
		return (realsize - sizeof(MMArrayPrefix)) / sizeof(T);
	}

	/**
	 * Gets the number of the elements actually being stored.
	 *
     * @return number of the elements actually being stored
     */
	unsigned long length() {
		return m_prefix->length;
	}

}; // MMArray

} // namespace MappedStructures

#endif // MAPPED_STRUCTURES_HPP