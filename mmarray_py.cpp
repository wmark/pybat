#include <Python.h>
#include <boost/python.hpp>

#include "MMArray.cpp"

using namespace boost::python;
using MappedStructures::FatalError;
using MappedStructures::IndexError;
using MappedStructures::MMArray;

template<class C>
static boost::shared_ptr<MMArray<C> > makeMMArray(const object& data)
{
	const char* filename = extract<const char*>(data);
	return boost::shared_ptr<MMArray<C> >(new MMArray<C>(filename));
}

void translateFE(FatalError const& e)
{
	PyErr_SetString(PyExc_RuntimeError, e.explanation.c_str());
}

void translateIE(IndexError const& e)
{
	PyErr_SetString(PyExc_IndexError, e.explanation.c_str());
}

BOOST_PYTHON_MODULE(MappedStructures)
{
	register_exception_translator<FatalError>(&translateFE);
	register_exception_translator<IndexError>(&translateIE);

#define PYTHON_MMARRAY(type, name) \
	class_<MMArray<type> >(name, no_init) \
		.def("__init__", make_constructor(makeMMArray<type>)) \
		.def("__setitem__", &MMArray<type>::set) \
		.def("__getitem__", &MMArray<type>::get) \
		.def("append", &MMArray<type>::append) \
		.def("close", &MMArray<type>::finish) \
		.def("expand", &MMArray<type>::expand) \
		.def("capacity", &MMArray<type>::capacity) \
		.def("__len__", &MMArray<type>::length)

	PYTHON_MMARRAY(int, "IntMMArray");
	PYTHON_MMARRAY(long long, "LongMMArray");
	PYTHON_MMARRAY(unsigned long long, "ULongMMArray");
	PYTHON_MMARRAY(float, "FloatMMArray");
	PYTHON_MMARRAY(double, "DoubleMMArray");

#undef PYTHON_MMARRAY
}
