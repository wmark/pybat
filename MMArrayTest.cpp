#include<cstdio>
#include "MMArray.cpp"
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE MMArrayTest
#include<boost/test/unit_test.hpp>

using MappedStructures::MMArray;
using MappedStructures::IndexError;

struct NewArrayEnv
{
	MMArray<int> my;
	unsigned int old_capacity, capacity;
	int x;

	NewArrayEnv()
	: old_capacity(0), capacity(0), x(121212), my(std::tmpnam(NULL))
	{
		old_capacity = capacity = my.capacity();
	}

	~NewArrayEnv()
	{
		remove(my.get_filename());
		my.finish();
	}
};

BOOST_FIXTURE_TEST_SUITE(MMArrayAll, NewArrayEnv)

BOOST_AUTO_TEST_CASE(NewArrayTestCase)
{
	BOOST_CHECK_GT(my.capacity(), 0);
}

BOOST_AUTO_TEST_CASE(GetSetTestCase)
{
	BOOST_REQUIRE_GT(my.capacity(), 0);
	for(unsigned int i = 0; i < capacity; i++) {
		x = -1000 + i * 10;
		BOOST_TEST_CHECKPOINT( "my.set with i=" << i << " and value=" << x );
		my.set(i, x);
	}
	BOOST_TEST_CHECKPOINT("Setting the ints is done.");
	BOOST_CHECK_EQUAL(my.capacity(), capacity);
	for(unsigned int i = 0; i < capacity; i++) {
		BOOST_TEST_CHECKPOINT( "my.set for i=" << i );
		x = my[i];
		BOOST_REQUIRE_EQUAL(-1000 + i * 10, x);
	}
}

BOOST_AUTO_TEST_CASE(ExceptionsTestCase)
{
	BOOST_CHECK_THROW(my.at(capacity + 1), IndexError);
	BOOST_CHECK_THROW(my.set(capacity + 1, x), IndexError);
}

BOOST_AUTO_TEST_CASE(ExpansionTestCase)
{
	BOOST_CHECK_THROW(my.set(capacity + 1, x), IndexError);
	my.expand(capacity + 1);
	BOOST_REQUIRE_GT(my.capacity(), old_capacity);
	BOOST_REQUIRE_NO_THROW(my.set(old_capacity + 1, x));
}

BOOST_AUTO_TEST_CASE(LengthTestCase)
{
	BOOST_REQUIRE_GT(my.capacity(), 16);
	BOOST_CHECK_EQUAL(my.length(), 0); // empty? then length should be 0
	my.set(0, x);
	BOOST_REQUIRE_EQUAL(my.length(), 1); // first element is set - length == 1
	my.set(1, x + 1);
	BOOST_REQUIRE_EQUAL(my.length(), 2); // another element gets appended, length should grow
	my.set(1, x + 2);
	BOOST_CHECK_EQUAL(my.length(), 2); // the previous entry got overwritten - length remains the same
	my.set(15, x + 3);
	BOOST_CHECK_EQUAL(my.length(), 16); // an element is written into the wild
}

BOOST_AUTO_TEST_CASE(AppendTestCase)
{
	BOOST_REQUIRE_GT(my.capacity(), 0);
	my.set(my.capacity() - 1, x);
	BOOST_CHECK_EQUAL(my.capacity(), old_capacity);
	BOOST_REQUIRE_EQUAL(my.length(), my.capacity());
	my.append(x);
	BOOST_REQUIRE_GT(my.capacity(), old_capacity);
}

BOOST_AUTO_TEST_CASE(FileTestCase)
{
	BOOST_REQUIRE(my.get_filename() != 0);
	BOOST_CHECK_NE(my.get_filename(), "/tmp/lots_of_ints");
}

BOOST_AUTO_TEST_SUITE_END()
